from __future__ import print_function

import math

# from IPython import display
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn import metrics
import tensorflow as tf

from InputFunctions import InputFunctions

tf.logging.set_verbosity(tf.logging.ERROR)
pd.options.display.max_rows = 10
pd.options.display.float_format = '{:.1f}'.format

# load our data set
california_housing_dataframe = pd.read_csv("https://download.mlcc.google.com/mledu-datasets/california_housing_train.csv", sep=",")

#  We'll randomize the data, just to be sure not to get any pathological ordering effects that might harm the
# performance of Stochastic Gradient Descent. Additionally, we'll scale median_house_value to be in units of thousands,
# so it can be learned a little more easily with learning rates in a range that we usually use.
california_housing_dataframe = california_housing_dataframe.reindex(
    np.random.permutation(california_housing_dataframe.index))
california_housing_dataframe["median_house_value"] /= 1000.0

# test print
# california_housing_dataframe.describe()
print("test1:\n" + str(california_housing_dataframe))

# Define the input feature: total_rooms.
my_feature = california_housing_dataframe[["total_rooms"]]
print("my feature:\n" + str(my_feature))

# Configure a numeric feature column for total_rooms. Actually it is a feature type.
feature_type = [tf.feature_column.numeric_column("total_rooms")]
print("feature_columns:\n" + str(feature_type))

# Define the target(label).
targets = california_housing_dataframe["median_house_value"]
print("targets:\n" + str(targets))

# Use gradient descent as the optimizer for training the model.
my_optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.0000001)
my_optimizer = tf.contrib.estimator.clip_gradients_by_norm(my_optimizer, 5.0)

# Configure the linear regression model with our feature columns and optimizer.
# Set a learning rate of 0.0000001 for Gradient Descent.
linear_regressor = tf.estimator.LinearRegressor(
    feature_columns=feature_type,
    optimizer=my_optimizer
)

train_result = linear_regressor.train(
    input_fn = lambda: InputFunctions.my_input_fn(my_feature, targets),
    steps=100
)
print("train_result: " + str(train_result))


# Create an input function for predictions.
# Note: Since we're making just one prediction for each example, we don't
# need to repeat or shuffle the data here.
prediction_input_fn =lambda: InputFunctions.my_input_fn(my_feature, targets, num_epochs=1, shuffle=False)

# Call predict() on the linear_regressor to make predictions.
predictions = linear_regressor.predict(input_fn=prediction_input_fn)

# Format predictions as a NumPy array, so we can calculate error metrics.
predictions = np.array([item['predictions'][0] for item in predictions])

# Print Mean Squared Error and Root Mean Squared Error.
mean_squared_error = metrics.mean_squared_error(predictions, targets)
root_mean_squared_error = math.sqrt(mean_squared_error)
print("Mean Squared Error (on training data): %0.3f" % mean_squared_error)
print("Root Mean Squared Error (on training data): %0.3f" % root_mean_squared_error)

# Mean Squared Error (MSE) can be hard to interpret, so we often look at Root Mean Squared Error (RMSE) instead.
# A nice property of RMSE is that it can be interpreted on the same scale as the original targets.
# Let's compare the RMSE to the difference of the min and max of our targets:
min_house_value = california_housing_dataframe["median_house_value"].min()
max_house_value = california_housing_dataframe["median_house_value"].max()
min_max_difference = max_house_value - min_house_value
print("Min. Median House Value: %0.3f" % min_house_value)
print("Max. Median House Value: %0.3f" % max_house_value)
print("Difference between Min. and Max.: %0.3f" % min_max_difference)
print("Root Mean Squared Error: %0.3f" % root_mean_squared_error)

# Our error spans nearly half the range of the target values. Can we do better?
# This is the question that nags at every model developer. Let's develop some basic strategies to reduce model error.
# The first thing we can do is take a look at how well our predictions match our targets, in terms of overall summary statistics.
calibration_data = pd.DataFrame()
calibration_data["predictions"] = pd.Series(predictions)
calibration_data["targets"] = pd.Series(targets)
calibration_data.describe()

# Okay, maybe this information is helpful. How does the mean value compare to the model's RMSE? How about the various quantiles?
# We can also visualize the data and the line we've learned. Recall that linear regression on a single feature can be drawn as a ' \
#                                               'line mapping input x to output y.
# First, we'll get a uniform random sample of the data so we can make a readable scatter plot.
sample = california_housing_dataframe.sample(n=300)

# Next, we'll plot the line we've learned, drawing from the model's bias term and feature weight, ' \
#                                                                'together with the scatter plot. The line will show up red.
# Get the min and max total_rooms values.
x_0 = sample["total_rooms"].min()
x_1 = sample["total_rooms"].max()
# Retrieve the final weight and bias generated during training.
weight = linear_regressor.get_variable_value('linear/linear_model/total_rooms/weights')[0]
bias = linear_regressor.get_variable_value('linear/linear_model/bias_weights')
# Get the predicted median_house_values for the min and max total_rooms values.
y_0 = weight * x_0 + bias
y_1 = weight * x_1 + bias
# Plot our regression line from (x_0, y_0) to (x_1, y_1).
plt.plot([x_0, x_1], [y_0, y_1], c='r')
# Label the graph axes.
plt.ylabel("median_house_value")
plt.xlabel("total_rooms")
# Plot a scatter plot from our data sample.
plt.scatter(sample["total_rooms"], sample["median_house_value"])
# Display graph.
plt.show()