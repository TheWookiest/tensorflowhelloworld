import tensorflow as tf

graph = tf.get_default_graph()
print(graph.get_operations())

input_value = tf.constant(1.0)
print(graph.get_operations())
print(input_value)

weight = tf.Variable(0.8)
print(graph.get_operations())
print(weight)

output_value = weight * input_value
print("1:" + str(output_value))

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)  # init all variables
result = sess.run(output_value)
print("2: " + str(result))
sess.close()

# Tensorboard
x = tf.constant(1.0, name='input')
w = tf.Variable(0.8, name='weight')
y = tf.multiply(w, x, name='output')
summary_writer = tf.summary.FileWriter('/home/olexii/Documents/nn/tensorboard/logs', sess.graph)
